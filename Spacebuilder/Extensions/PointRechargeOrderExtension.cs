﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using System;
using Tunynet.Common;
using Tunynet.PayServer;

namespace Tunynet.Spacebuilder
{
    /// <summary>
    /// 支付订单实体扩展
    /// </summary>
    public static class PointRechargeOrderExtension
    {
        /// <summary>
        /// 订单扩展到支付Model
        /// </summary>
        /// <param name="item">订单对象</param>
        /// <param name="notifyUrl">通知url</param>
        /// <param name="returnUrl">回调url</param>
        /// <returns></returns>
        public static PayData PointRechargeOrderAsPayEditModel(this PointRechargeOrder item, string notifyUrl, string returnUrl)
        {
            PayData payEditModel = new PayData()
            {
                Body = item.Description,
                Subject = item.Description,
                OutTradeNo = item.Id.ToString(),
                TotalAmount = item.Buyway == Buyway.WxPay ? (Math.Round(item.TotalPrice, 2) * 100).ToString() : item.TotalPrice.ToString("#0.00"),
                NotifyUrl = notifyUrl,
                ReturnUrl = returnUrl
            };
            return payEditModel;
        }
    }
}