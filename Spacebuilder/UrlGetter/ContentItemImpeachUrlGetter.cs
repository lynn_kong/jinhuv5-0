﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Tunynet.CMS;
using Tunynet.Common;

namespace Tunynet.Spacebuilder
{
    /// <summary>
    /// 资讯被举报详情url获取
    /// </summary>
    public class ContentItemImpeachUrlGetter : IImpeachUrlGetter
    {
        public ContentItemService contentItemService;

        public ContentItemImpeachUrlGetter(ContentItemService contentItemService)
        {
            this.contentItemService = contentItemService;
        }

        /// <summary>
        /// 租户类型Id
        /// </summary>
        public string TenantTypeId
        {
            get { return TenantTypeIds.Instance().ContentItem(); }
        }

        /// <summary>
        /// 获取被举报的对象实体
        /// </summary>
        /// <param name="reportObjectId">被举报相关对象Id</param>
        /// <returns></returns>
        public ImpeachObject GetImpeachObject(long reportObjectId)
        {
            var cms = contentItemService.Get(reportObjectId);
            if (cms != null)
            {
                ImpeachObject impeachObject = new ImpeachObject()
                {
                    Name = cms.Subject,
                    UserId = cms.UserId
                };
                if (cms.ContentModel.ModelKey == ContentModelKeys.Instance().Article())
                {
                    impeachObject.DetailUrl = SiteUrls.Instance().CMSDetail(cms.ContentItemId);
                }
                if (cms.ContentModel.ModelKey == ContentModelKeys.Instance().Image())
                {
                    impeachObject.DetailUrl = SiteUrls.Instance().CMSImgDetail(cms.ContentItemId);
                }
                if (cms.ContentModel.ModelKey == ContentModelKeys.Instance().Video())
                {
                    impeachObject.DetailUrl = SiteUrls.Instance().CMSVideoDetail(cms.ContentItemId);
                }
                return impeachObject;
            }
            return new ImpeachObject();
        }
    }
}