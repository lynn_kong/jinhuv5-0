﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Tunynet.Common;

namespace Tunynet.Spacebuilder
{
    /// <summary>
    /// 登陆触屏版url获取
    /// </summary>
    public class UserLoginTouchScreenlUrlGetter : ITouchScreenUrlGetter
    {
        /// <summary>
        /// 租户类型Id
        /// </summary>
        public string TenantTypeId
        {
            get { return TenantTypeIds.Instance().User(); }
        }

        /// <summary>
        /// 获取登陆详情的触屏版url
        /// </summary>
        /// <param name="objectId">对象ID</param>
        /// <returns></returns>
        public string GetTouchScreenDetailUrl(long objectId)
        {
            var touchScreenDetailUrl = "html/login.html?type=";
            var ipUrl = Utility.GetTouchScreenUrl();
            ipUrl = $"{ipUrl}/{touchScreenDetailUrl}{objectId}";
            return ipUrl;
        }
    }
}