﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Tunynet.Spacebuilder
{
    /// <summary>
    /// 后台用户积分任务
    /// </summary>
    public class UserPointTaskEditModel
    {
        /// <summary>
        /// 新建实体时使用
        /// </summary>
        public static UserPointTaskEditModel New()
        {
            UserPointTaskEditModel model = new UserPointTaskEditModel()
            {
                TaskName = string.Empty,
                Description = string.Empty,
                LinkUrl = string.Empty,
                Deadline = DateTime.Now.AddDays(30),
                IsUsing = true
            };
            return model;
        }

        /// <summary>
        /// 积分任务Id
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 积分任务种类Id
        /// </summary>
        public long TypeId { get; set; }

        /// <summary>
        /// 积分任务名
        /// </summary>
        [Required(ErrorMessage = "请输入任务名")]
        [StringLength(64, ErrorMessage = "最多可以输入64个字")]
        [Display(Name = "任务名称")]
        public string TaskName { get; set; }

        /// <summary>
        /// 任务说明
        /// </summary>
        [Required(ErrorMessage = "请输入任务说明")]
        [StringLength(512, ErrorMessage = "最多可以输入512个字")]
        [Display(Name = "任务说明")]
        public string Description { get; set; }

        /// <summary>
        /// 奖励积分值
        /// </summary>
        [Display(Name = "积分")]
        [RegularExpression("^[0-9]*[0-9][0-9]*$", ErrorMessage = "只能输入大于等于零的整数")]
        public int AwardPoints { get; set; }

        /// <summary>
        /// 奖励金币数
        /// </summary>
        [Display(Name = "金币")]
        [RegularExpression("^[0-9]*[0-9][0-9]*$", ErrorMessage = "只能输入大于等于零的整数")]
        public int AwardGolds { get; set; }

        /// <summary>
        /// 申请所需最小用户等级
        /// </summary>
        [Display(Name = "用户等级>=")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "只能输入整数")]
        public int MinUserRank { get; set; }

        /// <summary>
        /// 最小条件（人工审核任务时为0）
        /// </summary>
        [Display(Name = "最小条件")]
        [RegularExpression("^[0-9]*[1-9][0-9]*$", ErrorMessage = "只能输入大于零的整数")]
        public int MinCondition { get; set; }

        /// <summary>
        /// 截止时间
        /// </summary>
        [Display(Name = "任务截止时间")]
        public DateTime Deadline { get; set; } = DateTime.Now;

        /// <summary>
        /// 是否启用
        /// </summary>
        [Display(Name = "是否启用")]
        public bool IsUsing { get; set; }

        /// <summary>
        ///链接地址
        /// </summary>
        [Display(Name = "链接地址")]
        [Required(ErrorMessage = "请输入链接地址")]
        public string LinkUrl { get; set; }

        /// <summary>
        /// QQ分享
        /// </summary>
        [Display(Name = "QQ分享")]
        public bool ShareQQ { get; set; } = true;

        /// <summary>
        /// 微信分享
        /// </summary>
        [Display(Name = "微信分享")]
        public bool ShareWeiXin { get; set; } = true;

        /// <summary>
        /// 任务内容（人工审核任务、分享任务url）（多个任务内容json存储）
        /// </summary>
        public List<string> Options { get; set; }
    }
}