
SET FOREIGN_KEY_CHECKS=0;

START TRANSACTION;
-- 内容项升级
DELETE FROM `tn_ContentItems`;
INSERT `tn_ContentItems` (
       `ContentItemId`
      ,`ContentCategoryId`
      ,`ContentModelId`
      ,`Subject`
      ,`FeaturedImageAttachmentId`
      ,`DepartmentGuid`
      ,`Points`
      ,`UserId`
      ,`Author`
      ,`Body`
      ,`Summary`
      ,`IsLocked`
      ,`IsSticky`
      ,`ApprovalStatus`
      ,`IP`
      ,`DatePublished`
      ,`DateCreated`
      ,`LastModified`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT  
      `tn.jinhudemoNew`.`spb_cms_ContentItems`.`ContentItemId`
      ,
      `ContentFolderId` as `ContentFolderId`
      ,CASE  `IsContributed` 
	   WHEN  1 THEN 5
	   ELSE 1 END as `ContentModelId`
      ,`Title` as `Subject`
      ,`FeaturedImageAttachmentId`
	   ,'' as  `DepartmentGuid`
	   ,0 as `Points`
      ,`UserId`
      ,`Author`
	  ,`tn.jinhudemoNew`.`spb_cms_Addon_News`.Body as `Body`
      ,`Summary`
      ,`IsLocked`
	   , 0 as `IsSticky`
      ,`AuditStatus` as `ApprovalStatus`
      ,`IP`
      ,`ReleaseDate` as `DatePublished`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      , date_sub(`LastModified`,interval -8 hour)  as `LastModified`
      ,'IsVisible:S:0:4:' as `PropertyNames`
      ,'True' as `PropertyValues`
  FROM `tn.jinhudemoNew`.`spb_cms_ContentItems` left join  `tn.jinhudemoNew`.`spb_cms_Addon_News`  on `tn.jinhudemoNew`.`spb_cms_Addon_News`.ContentItemId=`tn.jinhudemoNew`.`spb_cms_ContentItems`.ContentItemId;
  
  -- 栏目升级
  
   DELETE FROM  `tn_ContentCategories`;
  INSERT  `tn_ContentCategories`( `CategoryId`
      ,`CategoryName`
      ,`Description`
      ,`ParentId`
      ,`ParentIdList`
      ,`ChildCount`
      ,`Depth`
      ,`IsEnabled`
      ,`ContentCount`
      ,`DateCreated`
      ,`ContentModelKeys`
      ,`ProcessDefinitionId`
      ,`DisplayOrder`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT `ContentFolderId` as `Cate;goryId`
      ,`FolderName` as `CategoryName`
      ,`Description`
      ,`ParentId`
      ,`ParentIdList`
      ,`ChildCount`
      ,`Depth`
      ,`IsEnabled`
      ,`ContentItemCount` as `ContentCount`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,'Article,Contribution'
	  ,0
      ,`DisplayOrder`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`. `spb_cms_ContentFolders`;

  -- 推荐内容升级
DELETE FROM `tn_SpecialContentItems`;

   -- 资讯精华升级
INSERT `tn_SpecialContentItems` (
       `TenantTypeId`
      ,`TypeId`
      ,`RegionId`
      ,`ItemId`
      ,`ItemName`
      ,`FeaturedImageAttachmentId`
     
      ,`RecommenderUserId`
      ,`DateCreated`
      ,`ExpiredDate`
      ,`DisplayOrder`
  )
SELECT  
      '101501' as `TenantTypeId`
      ,11 as `TypeId`
      ,0 as `RegionId`
      ,`ContentItemId`+1000000 as  `ItemId`
	  ,`Title` as `ItemName`
      , 0 as `FeaturedImageAttachmentId`
    
	  ,0 as `RecommenderUserId`
      ,  `DateCreated`
	  ,date_sub(NOW(),interval -20 year)  as `ExpiredDate`
	  , `ContentItemId` as  `DisplayOrder`
  FROM `tn.jinhudemoNew`.`spb_cms_contentitems` where `IsEssential`=1;

   -- 操作日志升级
  DELETE FROM `tn_OperationLogs`;
  
  INSERT `tn_OperationLogs` ( `TenantTypeId`, `OperationType`, `OperationObjectId`, `OperationObjectName`, `Description`, `OperationUserRole`, `OperationUserId`, `Operator`, `OperatorIP`, `AccessUrl`, `DateCreated`) 
SELECT 
	   CASE  `Source` 
	   WHEN  '贴吧' THEN '100003' 
	   WHEN  '贴子' THEN '100002'
	   WHEN  '资讯' THEN '100011'
	   WHEN  '评论' THEN '000031'
	   ELSE `Source` END as `TenantTypeId`
      ,`OperationType`
      ,`OperationObjectId`
	  ,`OperationObjectName`
      ,`Description`
	  ,' ' as `OperationUserRole`
      ,`OperatorUserId`
      ,`Operator`
      ,`OperatorIP`
      ,`AccessUrl`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
  FROM `tn.jinhudemoNew`.`tn_OperationLogs`
  where  `Source` = '资讯' ;


-- 附件升级
  DELETE FROM `tn_Attachments`;
INSERT `tn_Attachments` (`AttachmentId`, `AssociateId`, `OwnerId`, `TenantTypeId`, `UserId`, `UserDisplayName`, `FileName`, `FriendlyFileName`, `MediaType`, `ContentType`, `FileLength`, `Price`, `IP`, `ConvertStatus`, `DateCreated`, `Discription`, `IsShowInAttachmentList`, `PropertyNames`, `PropertyValues`, `DisplayOrder`) 
SELECT `AttachmentId`
      ,`AssociateId`
      ,`OwnerId`
      ,
	   CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`UserId`
      ,`UserDisplayName`
      ,`FileName`
      ,`FriendlyFileName`
      ,`MediaType`
      ,`ContentType`
      ,`FileLength`
      ,`Price`
      ,`IP`
	  ,0 as `ConvertStatus`
      ,`DateCreated`
	  ,'' as `Discription`
	  ,0 as `IsShowInAttachmentList`
      ,`PropertyNames`
      ,`PropertyValues`
	  ,`AttachmentId` as `DisplayOrder`
  FROM `tn.jinhudemoNew`.`tn_Attachments` where `TenantTypeId` in('101501') ;

   -- 收藏升级
  DELETE FROM `tn_Favorites`;
  insert `tn_Favorites`( 
      `TenantTypeId`
      ,`UserId`
      ,`ObjectId`)
SELECT   
       CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002'
	   WHEN  '101201' THEN '100003'  
	   WHEN  '101501' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`UserId`
      ,`ObjectId`
  FROM `tn.jinhudemoNew`.`tn_Favorites` where `TenantTypeId` in('101501') ;

 -- 评论升级
DELETE FROM `tn_Comments`;
INSERT `tn_Comments` (`Id`
      ,`ParentIds`
      ,`ParentId`
      ,`CommentedObjectId`
      ,`TenantTypeId`
     
      ,`ChildrenCount`
      ,`OwnerId`
      ,`UserId`
      ,`Author`
      ,`Subject`
      ,`Body`
      ,`IsAnonymous`
      ,`IsPrivate`
      ,`ApprovalStatus`
      ,`IP`
      ,`DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT  `Id`
      ,CASE  
	   WHEN  `ParentId`>0 THEN CONCAT('0,',`ParentId`,',')
	   ELSE '0,' END as `ParentIds`
      ,`ParentId`
      ,`CommentedObjectId`
      , CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
	
	  ,`ChildCount` as `ChildrenCount`
      ,`OwnerId`
      ,`UserId`
      ,`Author`
      ,`Subject`
      ,`Body`
      ,`IsAnonymous`
      ,`IsPrivate`
      ,`AuditStatus` as `ApprovalStatus`
      ,`IP`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`.`tn_Comments` where `TenantTypeId` in('101501') ;



-- 点赞升级
DELETE FROM `tn_Attitudes`;
INSERT `tn_Attitudes` (`Id`
      , `TenantTypeId`
      ,`ObjectId`
      ,`SupportCount`)
SELECT `Id`
       , CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`ObjectId`
      ,`SupportCount`
  FROM `tn.jinhudemoNew`.`tn_Attitudes` where `TenantTypeId` in('101501') ;
-- 点赞记录升级
DELETE FROM `tn_AttitudeRecords`;
INSERT `tn_AttitudeRecords` (`Id`
      ,`TenantTypeId`
      ,`ObjectId`
      ,`UserId`)
SELECT  `Id`
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`ObjectId`
      ,`UserId`
  FROM `tn.jinhudemoNew`.`tn_AttitudeRecords` where `TenantTypeId` in('101501') ;



-- 类别升级
DELETE FROM `tn_Categories`;
INSERT `tn_Categories` (`CategoryId`
      ,`ParentId`
      ,`OwnerId`
      ,`TenantTypeId`
      ,`CategoryName`
      ,`Description`
      ,`DisplayOrder`
      ,`Depth`
      ,`ChildCount`
      ,`ItemCount`
      ,`ImageAttachmentId`
      ,`LastModified`
      ,`DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT `CategoryId`
      ,`ParentId`
      ,`OwnerId`
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`CategoryName`
      ,`Description`
      ,`DisplayOrder`
      ,`Depth`
      ,`ChildCount`
      ,`ItemCount`
      ,0 as `ImageAttachmentId`
      ,`LastModified`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`.`tn_Categories` where `TenantTypeId` in('101501') ;


-- 类别和内容关联升级
DELETE FROM `tn_ItemsInCategories`;
INSERT `tn_ItemsInCategories` (`Id`
      ,`CategoryId`
      ,`ItemId`)
SELECT  `Id`
      ,`tn_ItemsInCategories`.`CategoryId`
      ,`ItemId`
  FROM `tn.jinhudemoNew`.`tn_ItemsInCategories` left join `tn.jinhudemoNew`.`tn_Categories`  on  `tn.jinhudemoNew`.`tn_Categories`.`CategoryId`  =`tn_ItemsInCategories`.`CategoryId` where  `tn.jinhudemoNew`.`tn_Categories`.TenantTypeId in('101501');


-- 标签升级
DELETE FROM `tn_Tags`;
INSERT `tn_Tags` (`TagId`
      ,`TenantTypeId`
      ,`TagName`
      ,`Description`
      ,`ImageAttachmentId`
      ,`IsFeatured`
      ,`ItemCount`
      ,`DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT `TagId`
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`TagName`
      ,`Description`
      ,0 as  `ImageAttachmentId`
      ,`IsFeatured`
      ,`ItemCount`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`.`tn_Tags` where `TenantTypeId` in('101501') ;

-- 标签关联项升级
DELETE FROM `tn_ItemsInTags`;
INSERT `tn_ItemsInTags` (`Id`
      ,`TagName`
      ,`ItemId`
      ,`TenantTypeId`)
SELECT  `Id`
      ,`TagName`
      ,`ItemId`
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
  FROM `tn.jinhudemoNew`.`tn_ItemsInTags` where `TenantTypeId` in('101501') ;

  -- 资讯热词搜索
delete from `tn_SearchWords` ;
INSERT .`tn_SearchWords` (`id`,
       `Word`
      ,`SearchTypeCode`
      ,`IsAddedByAdministrator`
      ,`DateCreated`
      ,`LastModified`)
	  SELECT  `id`,
      `Term` as `Word`
      ,'Cms' as `SearchTypeCode`
      ,`IsAddedByAdministrator`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`LastModified`
  FROM `tn.jinhudemoNew`.`tn_SearchedTerms` where SearchTypeCode = 'CmsSearcher';


  COMMIT;