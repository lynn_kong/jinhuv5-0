SET FOREIGN_KEY_CHECKS=0;

START TRANSACTION;
-- -- -问题升级
DELETE FROM`spb_Questions`;
INSERT`spb_Questions` (
       `QuestionId`
      ,`UserId`
      ,`Author`
      ,`Subject`
      ,`Body`
      ,`Reward`
      ,`AnswerCount`
      ,`LastAnswerDate`
      ,`Status`
      ,`ApprovalStatus`
      ,`IP`
      ,`DateCreated`
      ,`LastModified`
      ,`PropertyNames`
      ,`PropertyValues`
      ,`IsAnonymous`
      ,`IsDirectional`
      ,`IsSticky`)
SELECT  `QuestionId`
      ,`UserId`
      ,`Author`
      ,`Subject`
      ,`Body`
      ,`Reward`
      ,`AnswerCount`
      ,`LastAnswerDate`
      ,`Status`
      ,`AuditStatus` as `ApprovalStatus`
      ,`IP`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,date_sub(`LastModified`,interval -8 hour)  as `LastModified`
      ,`PropertyNames`
      ,`PropertyValues`
	  ,0 as `IsAnonymous`
	  ,0 as `IsDirectional`
      ,0 as  `IsSticky`
  FROM `tn.jinhudemoNew`.`spb_AskQuestions`;




-- -- -回答升级
DELETE FROM`spb_Answers`;

INSERT`spb_Answers` (
       `AnswerId`
      ,`QuestionId`
      ,`UserId`
      ,`Author`
      ,`Body`
      ,`IsBest`
      ,`ApprovalStatus`
      ,`IP`
      ,`DateCreated`
      ,`LastModified`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT `AnswerId`
      ,`QuestionId`
      ,`UserId`
      ,`Author`
      ,`Body`
      ,`IsBest`
      ,`AuditStatus` as `ApprovalStatus`
      ,`IP`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,date_sub(`LastModified`,interval -8 hour)  as `LastModified`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`.`spb_AskAnswers`;

 INSERT`tn_OperationLogs` ( `TenantTypeId`, `OperationType`, `OperationObjectId`, `OperationObjectName`, `Description`,`OperationUserId`, `Operator`, `OperatorIP`, `AccessUrl`, `DateCreated`) 
SELECT 
	   CASE  `Source` 
	   WHEN  '贴吧' THEN '100003' 
	   WHEN  '贴子' THEN '100002'
	   WHEN  '资讯' THEN '100011'
	   WHEN  '微博' THEN '100011'
	   WHEN  '评论' THEN '000031'
	   WHEN  '问答' THEN '101300'
	   ELSE `Source` END as `TenantTypeId`
      ,`OperationType`
      ,`OperationObjectId`
	  ,`OperationObjectName`
      ,`Description`
      ,`OperatorUserId` as OperationUserId
      ,`Operator`
      ,`OperatorIP`
      ,`AccessUrl`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
  FROM `tn.jinhudemoNew`.`tn_OperationLogs`
  where   `Source` = '问答';

     -- -- -附件升级
INSERT`tn_Attachments` (`AttachmentId`, `AssociateId`, `OwnerId`, `TenantTypeId`, `UserId`, `UserDisplayName`, `FileName`, `FriendlyFileName`, `MediaType`, `ContentType`, `FileLength`, `Price`, `IP`, `ConvertStatus`, `DateCreated`, `IsShowInAttachmentList`, `PropertyNames`, `PropertyValues`, `DisplayOrder`) 
SELECT `AttachmentId`
      ,`AssociateId`
      ,`OwnerId`
      ,
	   CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100101' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`UserId`
      ,`UserDisplayName`
      ,`FileName`
      ,`FriendlyFileName`
      ,`MediaType`
      ,`ContentType`
      ,`FileLength`
      ,`Price`
      ,`IP`
	  ,0 as `ConvertStatus`
      , `DateCreated`
	
	  ,0 as `IsShowInAttachmentList`
      ,`PropertyNames`
      ,`PropertyValues`
	  ,`AttachmentId` as `DisplayOrder`
  FROM `tn.jinhudemoNew`.`tn_Attachments` where `TenantTypeId` in('101301','101302');
  

      -- -- -收藏升级
  insert`tn_Favorites`( 
      `TenantTypeId`
      ,`UserId`
      ,`ObjectId`)
SELECT   
       CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002'
	   WHEN  '101201' THEN '100003'  
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100101' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`UserId`
      ,`ObjectId`
  FROM `tn.jinhudemoNew`.`tn_Favorites` where `TenantTypeId` in('101301','101302') ;

   -- -- -评论升级

INSERT`tn_Comments` (`Id`
    ,ParentIds
      ,`ParentId`
      ,`CommentedObjectId`
      ,`TenantTypeId`
      
      ,`ChildrenCount`
      ,`OwnerId`
      ,`UserId`
      ,`Author`
      ,`Subject`
      ,`Body`
      ,`IsAnonymous`
      ,`IsPrivate`
      ,`ApprovalStatus`
      ,`IP`
      ,`DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT  `Id`
      ,CASE  
	   WHEN  `ParentId`>0 THEN CONCAT('0,',`ParentId`,',')
	   ELSE '0,' END as `ParentIds`
      ,`ParentId`
      ,`CommentedObjectId`
      , CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100101' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
	 
	  ,`ChildCount` as `ChildrenCount`
      ,`OwnerId`
      ,`UserId`
      ,`Author`
      ,`Subject`
      ,`Body`
      ,`IsAnonymous`
      ,`IsPrivate`
      ,`AuditStatus` as `ApprovalStatus`
      ,`IP`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`.`tn_Comments` where `TenantTypeId` in('101301','101302') ;

    -- -- -点赞升级

INSERT`tn_Attitudes` (`Id`
      , `TenantTypeId`
      ,`ObjectId`
      ,`SupportCount`)
SELECT `Id`
       , CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100101' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`ObjectId`
      ,`SupportCount`
  FROM `tn.jinhudemoNew`.`tn_Attitudes` where `TenantTypeId` in('101301','101302') ;

-- -- -点赞记录升级

INSERT`tn_AttitudeRecords` (`Id`
      ,`TenantTypeId`
      ,`ObjectId`
      ,`UserId`)
SELECT  `Id`
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100101' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`ObjectId`
      ,`UserId`
  FROM `tn.jinhudemoNew`.`tn_AttitudeRecords` where `TenantTypeId` in('101301','101302') ;




-- -- -类别升级

INSERT`tn_Categories` (`CategoryId`
      ,`ParentId`
      ,`OwnerId`
      ,`TenantTypeId`
      ,`CategoryName`
      ,`Description`
      ,`DisplayOrder`
      ,`Depth`
      ,`ChildCount`
      ,`ItemCount`
      ,`ImageAttachmentId`
      ,`LastModified`
      ,`DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT `CategoryId`
      ,`ParentId`
      ,`OwnerId`
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100101' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`CategoryName`
      ,`Description`
      ,`DisplayOrder`
      ,`Depth`
      ,`ChildCount`
      ,`ItemCount`
      ,0 as `ImageAttachmentId`
      ,date_sub(`LastModified`,interval -8 hour)  as `LastModified`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`.`tn_Categories` where `TenantTypeId` in('101301','101302');


-- -- -类别和内容关联升级

INSERT`tn_ItemsInCategories` (`Id`
      ,`CategoryId`
      ,`ItemId`)
SELECT  `Id`
      ,`tn_ItemsInCategories`.`CategoryId`
      ,`ItemId`
  FROM `tn.jinhudemoNew`.`tn_ItemsInCategories` left join `tn.jinhudemoNew`.`tn_Categories`  on  `tn.jinhudemoNew`.`tn_Categories`.`CategoryId`  =`tn_ItemsInCategories`.`CategoryId` where  `tn.jinhudemoNew`.`tn_Categories`.TenantTypeId in('101301','101302');



-- -- -标签升级

INSERT`tn_Tags` (`TagId`
      ,`TenantTypeId`
      ,`TagName`
      ,`Description`
      ,`ImageAttachmentId`
      ,`IsFeatured`
      ,`ItemCount`
      ,`DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT `TagId`
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100101' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`TagName`
      ,`Description`
      ,0 as  `ImageAttachmentId`
      ,`IsFeatured`
      ,`ItemCount`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`.`tn_Tags` where `TenantTypeId` in('101301','101302') ;


-- -- -标签关联项升级

INSERT`tn_ItemsInTags` (`Id`
      ,`TagName`
      ,`ItemId`
      ,`TenantTypeId`)
SELECT  `Id`
      ,`TagName`
      ,`ItemId`
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
  FROM `tn.jinhudemoNew`.`tn_ItemsInTags` where `TenantTypeId` in('101301','101302') ;




-- -- -推荐问答精华升级
INSERT`tn_SpecialContentItems` (
       `TenantTypeId`
      ,`TypeId`
      ,`RegionId`
      ,`ItemId`
      ,`ItemName`
      ,`FeaturedImageAttachmentId`
      ,`Recommender`
      ,`RecommenderUserId`
      ,`DateCreated`
      ,`ExpiredDate`
      ,`DisplayOrder`
  )
SELECT 
      '101301' as `TenantTypeId`
      ,11 as `TypeId`
      ,0 as `RegionId`
      ,`QuestionId` as  `ItemId`
	   ,`Subject` as `ItemName`
	    , 0 as `FeaturedImageAttachmentId`
      ,' ' as `Recommender`
       ,0 as `RecommenderUserId`
      , date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
	  , date_sub(NOW(),interval -20 year) as `ExpiredDate`
	  , `QuestionId` as  `DisplayOrder`
     
  FROM `tn.jinhudemoNew`.`spb_AskQuestions` where `IsEssential`=1;

      -- 问答热词搜索
	  delete from `tn_SearchWords`  where SearchTypeCode = 'Ask';
INSERT .`tn_SearchWords` (
`id`,
       `Word`
      ,`SearchTypeCode`
      ,`IsAddedByAdministrator`
      ,`DateCreated`
      ,`LastModified`)
	  SELECT 
	  `id`, 
      `Term` as `Word`
      ,'Ask' as `SearchTypeCode`
      ,`IsAddedByAdministrator`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,date_sub(`LastModified`,interval -8 hour)  as `LastModified`
  FROM `tn.jinhudemoNew`.`tn_SearchedTerms` where SearchTypeCode = 'AskSearcher';


   DELETE FROM`tn_KvStore`;
drop procedure if exists StatisticStore;  
CREATE PROCEDURE StatisticStore()  
BEGIN  
    -- 创建接收游标数据的变量  
   DECLARE n BIGINT;
DECLARE pendingCount int;
DECLARE againCount int;
DECLARE successCount int;
DECLARE returnCount int;
DECLARE approvalStatus tinyint;
   
    -- 创建结束标志变量  
    declare done int default false;  
    -- 创建游标  
    declare cur cursor for SELECT  `UserId` FROM  `tn_Users`;  
    -- 指定游标循环结束时的返回值  
    declare continue HANDLER for not found set done = true;  

 -- 打开游标  
    open cur;  
    -- 开始循环游标里的数据  
    read_loop:loop  
    -- 根据游标当前指向的一条数据  
    fetch cur into n;  
    -- 判断游标的循环是否结束  
    if done then  
        leave read_loop;    -- 跳出游标循环  
    end if;  
    -- 获取一条数据时，将count值进行累加操作，这里可以做任意你想做的操作


   -- 提问计数升级
	set  returnCount= (select count(*) from  spb_Questions where UserId= n );
	set  successCount= (select count(*) from  spb_Questions where UserId= n  and spb_Questions.ApprovalStatus = 40);
    set  againCount= (select count(*) from  spb_Questions where UserId= n  and spb_Questions.ApprovalStatus = 30);
	set  pendingCount= (select count(*) from  spb_Questions where UserId= n  and spb_Questions.ApprovalStatus = 20);
	 	insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Ask-QuestionCount-UserId-',n),returnCount,NOW());
	insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Ask-QuestionCount-UserId-',n,'-AuditStatusSuccess'),successCount,NOW());
	  insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Ask-QuestionCount-UserId-',n,'-AuditStatusAgain'),againCount,NOW());
	  insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Ask-QuestionCount-UserId-',n,'-AuditStatusPending'),pendingCount,NOW());


	  -- 回答计数升级
	set   returnCount= (select count(*) from  spb_Answers where UserId=n );
	set   successCount= (select count(*) from  spb_Answers where UserId=n  and spb_Answers.ApprovalStatus = 40);
    set   againCount= (select count(*) from  spb_Answers where UserId=n  and spb_Answers.ApprovalStatus = 30);
	set   pendingCount= (select count(*) from  spb_Answers where UserId=n  and spb_Answers.ApprovalStatus = 20);
	 	insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Ask-AnswerCount-UserId-',n), returnCount,NOW());
	insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Ask-AnswerCount-UserId-',n,'-AuditStatusSuccess'), successCount,NOW());
	  insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Ask-AnswerCount-UserId-',n,'-AuditStatusAgain'), againCount,NOW());
	  insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Ask-AnswerCount-UserId-',n,'-AuditStatusPending'), pendingCount,NOW());
	 
	 
	
	   -- 最佳回答计数升级
	set   returnCount= (select count(*) from  spb_Answers where UserId=n  and IsBest =1 );
	
	 	insert   tn_KvStore
      (`Tkey`
      ,`TValue`
      ,`DateCreated`) values(CONCAT('Ask-AnswerAcceptCount-UserId-',n), returnCount,NOW());

	 


    -- 结束游标循环  
    end loop;  
    -- 关闭游标  
    close cur;  
  
    -- 输出结果  
  
END;  
-- 调用存储过程  
call StatisticStore();  

   COMMIT;