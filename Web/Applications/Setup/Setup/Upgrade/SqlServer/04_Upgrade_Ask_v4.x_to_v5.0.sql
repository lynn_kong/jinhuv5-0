begin tran



-----问题升级
DELETE FROM [dbo].[spb_Questions]
SET IDENTITY_INSERT [dbo].[spb_Questions] ON
INSERT [dbo].[spb_Questions] (
       [QuestionId]
      ,[UserId]
      ,[Author]
      ,[Subject]
      ,[Body]
      ,[Reward]
      ,[AnswerCount]
      ,[LastAnswerDate]
      ,[Status]
      ,[ApprovalStatus]
      ,[IP]
      ,[DateCreated]
      ,[LastModified]
      ,[PropertyNames]
      ,[PropertyValues]
      ,[IsAnonymous]
      ,[IsDirectional]
      ,[IsSticky])
SELECT  [QuestionId]
      ,[UserId]
      ,[Author]
      ,[Subject]
      ,[Body]
      ,[Reward]
      ,[AnswerCount]
      ,[LastAnswerDate]
      ,[Status]
      ,[AuditStatus] as [ApprovalStatus]
      ,[IP]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,dateadd(hour,8,[LastModified]) as [LastModified] 
      ,[PropertyNames]
      ,[PropertyValues]
	  ,0 as [IsAnonymous]
	  ,0 as [IsDirectional]
      ,0 as  [IsSticky]
  FROM [tn.jinhudemoNew].[dbo].[spb_AskQuestions]


SET IDENTITY_INSERT [dbo].[spb_Questions] OFF


-----回答升级
DELETE FROM [dbo].[spb_Answers]
SET IDENTITY_INSERT [dbo].[spb_Answers] ON
INSERT [dbo].[spb_Answers] (
       [AnswerId]
      ,[QuestionId]
      ,[UserId]
      ,[Author]
      ,[Body]
      ,[IsBest]
      ,[ApprovalStatus]
      ,[IP]
      ,[DateCreated]
      ,[LastModified]
      ,[PropertyNames]
      ,[PropertyValues])
SELECT [AnswerId]
      ,[QuestionId]
      ,[UserId]
      ,[Author]
      ,[Body]
      ,[IsBest]
      ,[AuditStatus] as [ApprovalStatus]
      ,[IP]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,dateadd(hour,8,[LastModified]) as [LastModified] 
      ,[PropertyNames]
      ,[PropertyValues]
  FROM [tn.jinhudemoNew].[dbo].[spb_AskAnswers]
SET IDENTITY_INSERT [dbo].[spb_Answers] OFF
 INSERT [dbo].[tn_OperationLogs] ( [TenantTypeId], [OperationType], [OperationObjectId], [OperationObjectName], [Description], [OperationUserRole], [OperationUserId], [Operator], [OperatorIP], [AccessUrl], [DateCreated]) 
SELECT 
	   CASE  [Source] 
	   WHEN  '贴吧' THEN '100003' 
	   WHEN  '贴子' THEN '100002'
	   WHEN  '资讯' THEN '100011'
	   WHEN  '微博' THEN '100011'
	   WHEN  '评论' THEN '000031'
	   WHEN  '问答' THEN '101300'
	   ELSE [Source] END as [TenantTypeId]
      ,[OperationType]
      ,[OperationObjectId]
	  ,[OperationObjectName]
      ,[Description]
	  ,' ' as [OperationUserRole]
      ,[OperatorUserId]
      ,[Operator]
      ,[OperatorIP]
      ,[AccessUrl]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
  FROM [tn.jinhudemoNew].[dbo].[tn_OperationLogs]
  where   [Source] = '问答'

     -----附件升级
 SET IDENTITY_INSERT [dbo].[tn_Attachments] ON
INSERT [dbo].[tn_Attachments] ([AttachmentId], [AssociateId], [OwnerId], [TenantTypeId], [UserId], [UserDisplayName], [FileName], [FriendlyFileName], [MediaType], [ContentType], [FileLength], [Price], [IP], [ConvertStatus], [DateCreated], [Discription], [IsShowInAttachmentList], [PropertyNames], [PropertyValues], [DisplayOrder]) 
SELECT [AttachmentId]
      ,[AssociateId]
      ,[OwnerId]
      ,
	   CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100101' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[UserId]
      ,[UserDisplayName]
      ,[FileName]
      ,[FriendlyFileName]
      ,[MediaType]
      ,[ContentType]
      ,[FileLength]
      ,[Price]
      ,[IP]
	  ,0 as [ConvertStatus]
      , [DateCreated]
	  ,'' as [Discription]
	  ,0 as [IsShowInAttachmentList]
      ,[PropertyNames]
      ,[PropertyValues]
	  ,[AttachmentId] as [DisplayOrder]
  FROM [tn.jinhudemoNew].[dbo].[tn_Attachments] where [TenantTypeId] in ('101301','101302') 
  
  SET IDENTITY_INSERT [dbo].[tn_Attachments] OFF
      -----收藏升级
  insert [dbo].[tn_Favorites]( 
      [TenantTypeId]
      ,[UserId]
      ,[ObjectId])
SELECT   
       CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002'
	   WHEN  '101201' THEN '100003'  
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100101' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[UserId]
      ,[ObjectId]
  FROM [tn.jinhudemoNew].[dbo].[tn_Favorites] where [TenantTypeId] in('101301','101302') 

   -----评论升级
SET IDENTITY_INSERT [dbo].[tn_Comments] ON
INSERT [dbo].[tn_Comments] ([Id]
      ,[ParentIds]
      ,[ParentId]
      ,[CommentedObjectId]
      ,[TenantTypeId]
      ,[CommentType]
      ,[ChildrenCount]
      ,[OwnerId]
      ,[UserId]
      ,[Author]
      ,[Subject]
      ,[Body]
      ,[IsAnonymous]
      ,[IsPrivate]
      ,[ApprovalStatus]
      ,[IP]
      ,[DateCreated]
      ,[PropertyNames]
      ,[PropertyValues])
SELECT  [Id]
         ,CASE  
	   WHEN  [ParentId]>0 THEN '0,'+(CAST([ParentId] as nvarchar(125)) +',')
	   ELSE '0,' END as [ParentIds] 
	  
      ,[ParentId]
      ,[CommentedObjectId]
      , CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100101' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
	  ,'' as [CommentType]
	  ,[ChildCount] as [ChildrenCount]
      ,[OwnerId]
      ,[UserId]
      ,[Author]
      ,[Subject]
      ,[Body]
      ,[IsAnonymous]
      ,[IsPrivate]
      ,[AuditStatus] as [ApprovalStatus]
      ,[IP]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[PropertyNames]
      ,[PropertyValues]
  FROM [tn.jinhudemoNew].[dbo].[tn_Comments] where [TenantTypeId] in('101301','101302') 
  SET IDENTITY_INSERT [dbo].[tn_Comments] OFF

    -----点赞升级
SET IDENTITY_INSERT [dbo].[tn_Attitudes] ON
INSERT [dbo].[tn_Attitudes] ([Id]
      , [TenantTypeId]
      ,[ObjectId]
      ,[SupportCount])
SELECT [Id]
       , CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100101' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[ObjectId]
      ,[SupportCount]
  FROM [tn.jinhudemoNew].[dbo].[tn_Attitudes] where [TenantTypeId] in('101301','101302') 
SET IDENTITY_INSERT [dbo].[tn_Attitudes] OFF
-----点赞记录升级
SET IDENTITY_INSERT [dbo].[tn_AttitudeRecords] ON
INSERT [dbo].[tn_AttitudeRecords] ([Id]
      ,[TenantTypeId]
      ,[ObjectId]
      ,[UserId])
SELECT  [Id]
      ,CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100101' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[ObjectId]
      ,[UserId]
  FROM [tn.jinhudemoNew].[dbo].[tn_AttitudeRecords] where [TenantTypeId] in('101301','101302') 
SET IDENTITY_INSERT [dbo].[tn_AttitudeRecords] OFF



-----类别升级
SET IDENTITY_INSERT [dbo].[tn_Categories] ON
INSERT [dbo].[tn_Categories] ([CategoryId]
      ,[ParentId]
      ,[OwnerId]
      ,[TenantTypeId]
      ,[CategoryName]
      ,[Description]
      ,[DisplayOrder]
      ,[Depth]
      ,[ChildCount]
      ,[ItemCount]
      ,[ImageAttachmentId]
      ,[LastModified]
      ,[DateCreated]
      ,[PropertyNames]
      ,[PropertyValues])
SELECT [CategoryId]
      ,[ParentId]
      ,[OwnerId]
      ,CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100101' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[CategoryName]
      ,[Description]
      ,[DisplayOrder]
      ,[Depth]
      ,[ChildCount]
      ,[ItemCount]
      ,0 as [ImageAttachmentId]
      ,[LastModified]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[PropertyNames]
      ,[PropertyValues]
  FROM [tn.jinhudemoNew].[dbo].[tn_Categories] where [TenantTypeId] in('101301','101302') 
SET IDENTITY_INSERT [dbo].[tn_Categories] OFF

-----类别和内容关联升级
SET IDENTITY_INSERT [dbo].[tn_ItemsInCategories] ON
INSERT [dbo].[tn_ItemsInCategories] ([Id]
      ,[CategoryId]
      ,[ItemId])
SELECT  [Id]
      ,[tn_ItemsInCategories].[CategoryId]
      ,[ItemId]
  FROM [tn.jinhudemoNew].[dbo].[tn_ItemsInCategories] left join [tn.jinhudemoNew].[dbo].[tn_Categories]  on  [tn.jinhudemoNew].[dbo].[tn_Categories].[CategoryId]  =[tn_ItemsInCategories].[CategoryId] where  [tn.jinhudemoNew].[dbo].[tn_Categories].TenantTypeId in('101301','101302')
SET IDENTITY_INSERT [dbo].[tn_ItemsInCategories] OFF


-----标签升级
SET IDENTITY_INSERT [dbo].[tn_Tags] ON
INSERT [dbo].[tn_Tags] ([TagId]
      ,[TenantTypeId]
      ,[TagName]
      ,[Description]
      ,[ImageAttachmentId]
      ,[IsFeatured]
      ,[ItemCount]
      ,[DateCreated]
      ,[PropertyNames]
      ,[PropertyValues])
SELECT [TagId]
      ,CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100101' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[TagName]
      ,[Description]
      ,0 as  [ImageAttachmentId]
      ,[IsFeatured]
      ,[ItemCount]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[PropertyNames]
      ,[PropertyValues]
  FROM [tn.jinhudemoNew].[dbo].[tn_Tags] where [TenantTypeId] in('101301','101302') 
SET IDENTITY_INSERT [dbo].[tn_Tags] OFF

-----标签关联项升级
SET IDENTITY_INSERT [dbo].[tn_ItemsInTags] ON
INSERT [dbo].[tn_ItemsInTags] ([Id]
      ,[TagName]
      ,[ItemId]
      ,[TenantTypeId])
SELECT  [Id]
      ,[TagName]
      ,[ItemId]
      ,CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
  FROM [tn.jinhudemoNew].[dbo].[tn_ItemsInTags] where [TenantTypeId] in('101301','101302') 
SET IDENTITY_INSERT [dbo].[tn_ItemsInTags] OFF



-----推荐问答精华升级
INSERT [dbo].[tn_SpecialContentItems] (
       [TenantTypeId]
      ,[TypeId]
      ,[RegionId]
      ,[ItemId]
      ,[ItemName]
      ,[FeaturedImageAttachmentId]
      ,[Recommender]
      ,[RecommenderUserId]
      ,[DateCreated]
      ,[ExpiredDate]
      ,[DisplayOrder]
  )
SELECT 
      '101301' as [TenantTypeId]
      ,11 as [TypeId]
      ,0 as [RegionId]
      ,[QuestionId] as  [ItemId]
	   ,[Subject] as [ItemName]
	    , 0 as [FeaturedImageAttachmentId]
      ,' ' as [Recommender]
       ,0 as [RecommenderUserId]
      , dateadd(hour,8,[DateCreated]) as [DateCreated] 
	  , dateadd(y,20,getdate()) as [ExpiredDate]
	  , [QuestionId] as  [DisplayOrder]
     
  FROM [tn.jinhudemoNew].[dbo].[spb_AskQuestions] where IsEssential=1



   -----问答热词更新
   delete from [tn_SearchWords]  where SearchTypeCode = 'Ask'
     SET IDENTITY_INSERT [dbo].[tn_SearchWords] ON
INSERT [dbo].[tn_SearchWords] (
id,
       [Word]
      ,[SearchTypeCode]
      ,[IsAddedByAdministrator]
      ,[DateCreated]
      ,[LastModified])
	  SELECT  id,
      [Term] as [Word]
      ,'Ask' as [SearchTypeCode]
      ,[IsAddedByAdministrator]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[LastModified]
  FROM [tn.jinhudemoNew].[dbo].[tn_SearchedTerms] where SearchTypeCode = 'AskSearcher'

    SET IDENTITY_INSERT [dbo].[tn_SearchWords] OFF


DELETE FROM [dbo].[tn_KvStore]

DECLARE @userId nvarchar(60)
DECLARE @pendingCount int
DECLARE @againCount int
DECLARE @successCount int
DECLARE @returnCount int
DECLARE @approvalStatus tinyint
DECLARE My_Cursor CURSOR --定义游标
FOR (SELECT  [UserId] FROM  [tn_Users]) --查出需要的集合放到游标中
OPEN My_Cursor; --打开游标 
FETCH NEXT FROM My_Cursor into @userId;  --读取第一行数据
WHILE @@FETCH_STATUS = 0


    BEGIN
	--提问计数升级
	set  @returnCount= (select count(*) from  spb_Questions where UserId= @userId )
	set  @successCount= (select count(*) from  spb_Questions where UserId= @userId  and ApprovalStatus = 40)
    set  @againCount= (select count(*) from  spb_Questions where UserId= @userId  and ApprovalStatus = 30)
	set  @pendingCount= (select count(*) from  spb_Questions where UserId= @userId  and ApprovalStatus = 20)
	 	insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Ask-QuestionCount-UserId-'+@userId,@returnCount,getdate())
	insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Ask-QuestionCount-UserId-'+@userId+'-AuditStatusSuccess',@successCount,getdate())
	  insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Ask-QuestionCount-UserId-'+@userId+'-AuditStatusAgain',@againCount,getdate())
	  insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Ask-QuestionCount-UserId-'+@userId+'-AuditStatusPending',@pendingCount,getdate())


	  --回答计数升级
	set  @returnCount= (select count(*) from  spb_Answers where UserId= @userId )
	set  @successCount= (select count(*) from  spb_Answers where UserId= @userId  and ApprovalStatus = 40)
    set  @againCount= (select count(*) from  spb_Answers where UserId= @userId  and ApprovalStatus = 30)
	set  @pendingCount= (select count(*) from  spb_Answers where UserId= @userId  and ApprovalStatus = 20)
	 	insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Ask-AnswerCount-UserId-'+@userId,@returnCount,getdate())
	insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Ask-AnswerCount-UserId-'+@userId+'-AuditStatusSuccess',@successCount,getdate())
	  insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Ask-AnswerCount-UserId-'+@userId+'-AuditStatusAgain',@againCount,getdate())
	  insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Ask-AnswerCount-UserId-'+@userId+'-AuditStatusPending',@pendingCount,getdate())
	 
	 
	
	   --最佳回答计数升级
	set  @returnCount= (select count(*) from  spb_Answers where UserId= @userId  and IsBest =1 )
	
	 	insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Ask-AnswerAcceptCount-UserId-'+@userId,@returnCount,getdate())

	 
	  
        FETCH NEXT FROM My_Cursor  into @userId;   --读取下一行数据 
    END
CLOSE My_Cursor; --关闭游标
DEALLOCATE My_Cursor; --释放游标
   commit tran