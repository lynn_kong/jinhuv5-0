 begin tran
 
 -----操作日志升级

INSERT [dbo].[tn_OperationLogs] ( [TenantTypeId], [OperationType], [OperationObjectId], [OperationObjectName], [Description], [OperationUserRole], [OperationUserId], [Operator], [OperatorIP], [AccessUrl], [DateCreated]) 
SELECT 
	   CASE  [Source] 
	   WHEN  '贴吧' THEN '100003' 
	   WHEN  '贴子' THEN '100002'
	   WHEN  '资讯' THEN '100011'
	   WHEN  '评论' THEN '000031'
	   ELSE [Source] END as [TenantTypeId]
      ,[OperationType]
      ,[OperationObjectId]
	  ,[OperationObjectName]
      ,[Description]
	  ,' ' as [OperationUserRole]
      ,[OperatorUserId]
      ,[Operator]
      ,[OperatorIP]
      ,[AccessUrl]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
  FROM [tn.jinhudemoNew].[dbo].[tn_OperationLogs]
  where   [Source] = '评论'
   
  -----附件下载记录升级
DELETE FROM [dbo].[tn_AttachmentAccessRecords]
SET IDENTITY_INSERT [dbo].[tn_AttachmentAccessRecords] ON
insert [dbo].[tn_AttachmentAccessRecords]( [Id]
      ,[AttachmentId]
      ,[AccessType]
      ,[UserId]
      ,[UserDisplayName]
      ,[Price]
      ,[LastDownloadDate]
      ,[DownloadDate]
      ,[IP])
	  SELECT  [Id]
      ,[AttachmentId]
      ,0 as [AccessType]
      ,[UserId]
      ,[UserDisplayName]
      ,[Price]
      ,dateadd(hour,8,[LastDownloadDate]) as [LastDownloadDate] 
      ,dateadd(hour,8,[DownloadDate]) as [DownloadDate] 
      ,[IP]
  FROM [tn.jinhudemoNew].[dbo].[tn_AttachmentDownloadRecords]


SET IDENTITY_INSERT [dbo].[tn_AttachmentAccessRecords] OFF
 
-----友情链接升级
DELETE FROM [dbo].[tn_Links]
SET IDENTITY_INSERT [dbo].[tn_Links] ON
INSERT [dbo].[tn_Links] ([LinkId], [LinkName], [LinkUrl], [ImageAttachmentId], [Description], [IsEnabled], [DisplayOrder], [DateCreated], [PropertyNames], [PropertyValues])
SELECT  [LinkId]
      ,[LinkName]
      ,[LinkUrl]
	  ,0 as [ImageAttachmentId]
      ,[Description]
      ,[IsEnabled]
      ,[DisplayOrder]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[PropertyNames]
      ,[PropertyValues]
  FROM [tn.jinhudemoNew].[dbo].[spb_Links]
SET IDENTITY_INSERT [dbo].[tn_Links] OFF


-----友情链接类别升级
SET IDENTITY_INSERT [dbo].[tn_Categories] ON
INSERT [dbo].[tn_Categories] ([CategoryId]
      ,[ParentId]
      ,[OwnerId]
      ,[TenantTypeId]
      ,[CategoryName]
      ,[Description]
      ,[DisplayOrder]
      ,[Depth]
      ,[ChildCount]
      ,[ItemCount]
      ,[ImageAttachmentId]
      ,[LastModified]
      ,[DateCreated]
      ,[PropertyNames]
      ,[PropertyValues])
SELECT [CategoryId]
      ,[ParentId]
      ,[OwnerId]
      ,CASE  [TenantTypeId] 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100201' THEN '100011' 
	   ELSE [TenantTypeId] END as [TenantTypeId]
      ,[CategoryName]
      ,[Description]
      ,[DisplayOrder]
      ,[Depth]
      ,[ChildCount]
      ,[ItemCount]
      ,0 as [ImageAttachmentId]
      ,[LastModified]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[PropertyNames]
      ,[PropertyValues]
  FROM [tn.jinhudemoNew].[dbo].[tn_Categories] where [TenantTypeId] in('000071') 
SET IDENTITY_INSERT [dbo].[tn_Categories] OFF
-----类别和内容关联升级
SET IDENTITY_INSERT [dbo].[tn_ItemsInCategories] ON
INSERT [dbo].[tn_ItemsInCategories] ([Id]
      ,[CategoryId]
      ,[ItemId])
SELECT  [Id]
      ,[tn_ItemsInCategories].[CategoryId]
      ,[ItemId]
  FROM [tn.jinhudemoNew].[dbo].[tn_ItemsInCategories] left join [tn.jinhudemoNew].[dbo].[tn_Categories]  on  [tn.jinhudemoNew].[dbo].[tn_Categories].[CategoryId]  =[tn_ItemsInCategories].[CategoryId] where  [tn.jinhudemoNew].[dbo].[tn_Categories].TenantTypeId in('000071')
SET IDENTITY_INSERT [dbo].[tn_ItemsInCategories] OFF
-----用户升级
DELETE FROM [dbo].[tn_Users]
insert  [dbo].[tn_Users] ([UserId]
      ,[UserName]
      ,[Password]
      ,[HasAvatar]
      ,[HasCover]
      ,[PasswordFormat]
      ,[AccountEmail]
      ,[IsEmailVerified]
      ,[AccountMobile]
      ,[IsMobileVerified]
      ,[TrueName]
      ,[ForceLogin]
      ,[Status]
      ,[DateCreated]
      ,[IpCreated]
      ,[UserType]
      ,[LastActivityTime]
      ,[LastAction]
      ,[IpLastActivity]
      ,[IsBanned]
      ,[BanReason]
      ,[BanDeadline]
      ,[IsModerated]
      ,[IsForceModerated]
      ,[DatabaseQuota]
      ,[DatabaseQuotaUsed]
      ,[IsUseCustomStyle]
      ,[FollowedCount]
      ,[FollowerCount]
      ,[ExperiencePoints]
      ,[ReputationPoints]
      ,[TradePoints]
      ,[TradePoints2]
      ,[TradePoints3]
      ,[TradePoints4]
      ,[FrozenTradePoints]
      ,[Rank]
      ,[AuditStatus]
      ,[UserGuid])

SELECT  [UserId]
      ,[UserName]
      ,[Password]
	  ,  CASE len([Avatar])
	   WHEN 11 THEN 0
	    WHEN 13 THEN 0
		 WHEN 14 THEN 0
	   ELSE 1 END as [HasAvatar]
	   ,0 as [HasCover]
      ,[PasswordFormat]
      ,[AccountEmail]
      ,[IsEmailVerified]
      ,[AccountMobile]
      ,[IsMobileVerified]
      ,[TrueName]
      ,[ForceLogin]
      ,[IsActivated]as [Status]
      ,[DateCreated]
      ,[IpCreated]
      ,[UserType]
      ,[LastActivityTime]
      ,[LastAction]
      ,[IpLastActivity]
      ,[IsBanned]
      ,[BanReason]
      ,[BanDeadline]
      ,[IsModerated]
      ,[IsForceModerated]
      ,[DatabaseQuota]
      ,[DatabaseQuotaUsed]
      ,[IsUseCustomStyle]
      ,[FollowedCount]
      ,[FollowerCount]
      ,[ExperiencePoints]
      ,[ReputationPoints]
      ,[TradePoints]
      ,[TradePoints2]
      ,[TradePoints3]
      ,[TradePoints4]
      ,[FrozenTradePoints]
      ,[Rank]
	  ,40 as [AuditStatus]
      ,'' as [UserGuid]
  FROM [tn.jinhudemoNew].[dbo].[tn_Users]
-----用户资料升级
DELETE FROM [dbo].[spb_UserProfiles ]
INSERT [dbo].[spb_UserProfiles ] ([UserId], [Gender], [BirthdayType], [Birthday], [LunarBirthday], [NowAreaCode], [QQ], [CardType], [CardID], [Introduction], [PropertyNames], [PropertyValues], [Integrity]) 
SELECT [UserId]
      ,[Gender]
      ,[BirthdayType]
      ,[Birthday]
      ,[LunarBirthday]
      ,[NowAreaCode]
      ,[QQ]
      ,[CardType]
      ,[CardID]
      ,[Introduction]
      ,[PropertyNames]
      ,[PropertyValues]
      ,[Integrity]
  FROM [tn.jinhudemoNew].[dbo].[spb_Profiles]
  -----第三方账号绑定升级
DELETE FROM [dbo].[tn_AccountBindings]
SET IDENTITY_INSERT [dbo].[tn_AccountBindings] ON
INSERT [dbo].[tn_AccountBindings] ([Id]
      ,[UserId]
      ,[AccountTypeKey]
      ,[Identification]
      ,[AccessToken]
	  ,[ExpiredDate] )
	  SELECT  [Id]
      ,[UserId]
      ,[AccountTypeKey]
      ,[Identification]
      ,[AccessToken]
	  ,getdate() as [ExpiredDate]	
	   FROM [tn.jinhudemoNew].[dbo].[tn_AccountBindings]
 SET IDENTITY_INSERT [dbo].[tn_AccountBindings] OFF
   -----第三方账号类型升级
DELETE FROM [dbo].[tn_AccountTypes]
INSERT [dbo].[tn_AccountTypes] ( [AccountTypeKey]
      ,[ThirdAccountGetterClassType]
      ,[AppKey]
      ,[AppSecret]
      ,[IsEnabled])
SELECT [AccountTypeKey]
      ,'Tunynet.Spacebuilder.QQAccountGetter,Tunynet.AccountBindings' as [ThirdAccountGetterClassType]
      ,[AppKey]
      ,[AppSecret]
      ,[IsEnabled]
  FROM [tn.jinhudemoNew].[dbo].[tn_AccountTypes] where  [tn_AccountTypes].[AccountTypeKey] = 'QQ'
    -----关注升级
DELETE FROM [dbo].[tn_Follows]
 SET IDENTITY_INSERT [dbo].[tn_Follows] ON
INSERT [dbo].[tn_Follows] ( [Id]
      ,[UserId]
      ,[FollowedUserId]
      ,[NoteName]
      ,[IsQuietly]
      ,[IsNewFollower]
      ,[DateCreated]
      ,[PropertyNames]
      ,[PropertyValues]
      ,[IsMutual]
      ,[LastContactDate])
	  SELECT  [Id]
      ,[UserId]
      ,[FollowedUserId]
      ,[NoteName]
      ,[IsQuietly]
      ,[IsNewFollower]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[PropertyNames]
      ,[PropertyValues]
      ,[IsMutual]
      ,[LastContactDate]
  FROM [tn.jinhudemoNew].[dbo].[tn_Follows]
SET IDENTITY_INSERT [dbo].[tn_Follows] OFF

-----邀请码升级
DELETE FROM [dbo].[tn_InvitationCodes]
INSERT [dbo].[tn_InvitationCodes] ( [Code]
      ,[UserId]
      ,[IsMultiple]
      ,[ExpiredDate]
      ,[DateCreated])
	 SELECT [Code]
      ,[UserId]
      ,[IsMultiple]
      ,[ExpiredDate]
      ,[DateCreated]
  FROM [tn.jinhudemoNew].[dbo].[tn_InvitationCodes]

-----邀请好友记录升级
DELETE FROM [dbo].[tn_InviteFriendRecords]
 SET IDENTITY_INSERT [dbo].[tn_InviteFriendRecords] ON
INSERT [dbo].[tn_InviteFriendRecords] ( [Id]
      ,[UserId]
      ,[InvitedUserId]
      ,[Code]
      ,[DateCreated]
      ,[IsRewarded])
SELECT [Id]
      ,[UserId]
      ,[InvitedUserId]
      ,[Code]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
      ,[InvitingUserHasBeingRewarded] as [IsRewarded]
  FROM [tn.jinhudemoNew].[dbo].[tn_InviteFriendRecords]
SET IDENTITY_INSERT [dbo].[tn_InviteFriendRecords] OFF

-----积分记录升级
DELETE FROM [dbo].[tn_PointRecords]
 SET IDENTITY_INSERT [dbo].[tn_PointRecords] ON
INSERT [dbo].[tn_PointRecords] ( [RecordId]
      ,[UserId]
      ,[OperatorUserId]
      ,[PointItemName]
      ,[Description]
      ,[ExperiencePoints]
      ,[ReputationPoints]
      ,[TradePoints]
      ,[TradePoints2]
      ,[TradePoints3]
      ,[TradePoints4]
      ,[DateCreated])
SELECT [RecordId]
      ,[UserId]
	  ,0 as [OperatorUserId]
      ,[PointItemName]
      ,[Description]
      ,[ExperiencePoints]
      ,[ReputationPoints]
      ,[TradePoints]
      ,[TradePoints2]
      ,[TradePoints3]
      ,[TradePoints4]
      ,dateadd(hour,8,[DateCreated]) as [DateCreated] 
  FROM [tn.jinhudemoNew].[dbo].[tn_PointRecords]
SET IDENTITY_INSERT [dbo].[tn_PointRecords] OFF


-----全局热词更新
delete from [tn_SearchWords]  where SearchTypeCode = 'All'
SET IDENTITY_INSERT [dbo].[tn_SearchWords] ON
INSERT [dbo].[tn_SearchWords] (
       Id,
       [Word]
      ,[SearchTypeCode]
      ,[IsAddedByAdministrator]
      ,[DateCreated]
      ,[LastModified])
	  SELECT  
	  Id,
      [Term] as [Word]
      ,'All' as [SearchTypeCode]
      ,[IsAddedByAdministrator]
      , dateadd(hour,8,[DateCreated]) as [DateCreated]
      ,[LastModified]
  FROM [tn.jinhudemoNew].[dbo].[tn_SearchedTerms] where SearchTypeCode = 'GlobalSearcher'
  SET IDENTITY_INSERT [dbo].[tn_SearchWords] OFF
  

-------通知升级
--DELETE FROM [dbo].[tn_Notices]
--SET IDENTITY_INSERT [dbo].[tn_Notices] ON
--INSERT [dbo].[tn_Notices] ([Id]
--      ,[NoticeTypeKey]
--      ,[ReceiverId]
--      ,[LeadingActorUserId]
--      ,[LeadingActor]
--      ,[RelativeObjectName]
--      ,[RelativeObjectId]
--      ,[RelativeObjectUrl]
--      ,[Body]
--      ,[Status]
--      ,[DateCreated]
--      ,[Times]
--      ,[PropertyNames]
--      ,[PropertyValues]
--      ,[LastSendDate]
--      ,[ObjectId])
--SELECT  [Id]
--      ,[TemplateName] as [NoticeTypeKey]  
--      ,[UserId] as [ReceiverId]
--      ,[LeadingActorUserId]
--      ,[LeadingActor]
--      ,[RelativeObjectName]
--      ,[RelativeObjectId]
--	   ,[RelativeObjectUrl]
--      ,[Body]
--      ,[Status]
--      ,[DateCreated]
--	   ,0 as [Times]
--      ,[PropertyNames]
--      ,[PropertyValues]
--	  ,getdate() as [LastSendDate]
--      , 0 as [ObjectId]
--  FROM [tn.jinhudemoNew].[dbo].[tn_Notices]
--SET IDENTITY_INSERT [dbo].[tn_Notices] OFF

-----管理员角色保留初始管理员
DELETE FROM [dbo].[tn_UsersInRoles]
SET IDENTITY_INSERT [dbo].[tn_UsersInRoles] ON
INSERT [dbo].[tn_UsersInRoles] (
       [Id]
      ,[UserId]
      ,[RoleId])
SELECT  [Id]
      ,[UserId]
      ,101 as [RoleId]
  FROM [tn.jinhudemoNew].[dbo].[tn_UsersInRoles] where  RoleName='SuperAdministrator'
SET IDENTITY_INSERT [dbo].[tn_UsersInRoles] OFF

-------计数升级
DELETE FROM [dbo].[tn_Counts]
DECLARE @tenantTypeId varchar(50)
DECLARE @TypeId int
DECLARE My_Cursor CURSOR --定义游标
FOR (select name from	[tn.jinhudemoNew]..sysobjects where xtype='u'and name in ('tn_Counts_000031','tn_Counts_101202','tn_Counts_101301','tn_Counts_101302','tn_Counts_101501')) --查出需要的集合放到游标中
OPEN My_Cursor; --打开游标
FETCH NEXT FROM My_Cursor into @tenantTypeId; --读取第一行数据
WHILE @@FETCH_STATUS = 0
    BEGIN
	 set @TypeId =SUBSTRING(@tenantTypeId,11,6)
	  set @TypeId = 	CASE @TypeId 
     	WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE @TypeId END
	INSERT [dbo].[tn_Counts] (
       [OwnerId]
      ,[ObjectId]
      ,[CountType]
      ,[StatisticsCount] 
	  ,[TenantTypeId])
	 exec ('  select  [OwnerId],[ObjectId],[CountType],[StatisticsCount], '+@TypeId+' as TenantTypeId from [tn.jinhudemoNew].[dbo].'+@tenantTypeId) 
        FETCH NEXT FROM My_Cursor into @tenantTypeId; --读取下一行数据
    END
CLOSE My_Cursor; --关闭游标
DEALLOCATE My_Cursor; --释放游标
-------每日计数升级
DELETE FROM [dbo].[tn_CountsPerDay]

DECLARE My_Cursor CURSOR --定义游标
FOR (select name from	[tn.jinhudemoNew]..sysobjects where xtype='u'and name in ('tn_CountsPerDay_000031','tn_CountsPerDay_101202','tn_CountsPerDay_101301','tn_CountsPerDay_101302','tn_CountsPerDay_101501')) --查出需要的集合放到游标中
OPEN My_Cursor; --打开游标
FETCH NEXT FROM My_Cursor into @tenantTypeId;  --读取第一行数据
WHILE @@FETCH_STATUS = 0
    BEGIN
	  set @TypeId =SUBSTRING(@tenantTypeId,17,6)
	   set @TypeId = 	CASE @TypeId 
     	WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE @TypeId  END
	INSERT [dbo].[tn_CountsPerDay] (
      [OwnerId]
      ,[ObjectId]
      ,[CountType]
      ,[ReferenceYear]
      ,[ReferenceMonth]
      ,[ReferenceDay]
      ,[StatisticsCount],
	  [TenantTypeId])
	 exec ('  select  [OwnerId],[ObjectId],[CountType],[ReferenceYear],[ReferenceMonth],[ReferenceDay],[StatisticsCount],'+@TypeId+' as TenantTypeId from [tn.jinhudemoNew].[dbo].'+@tenantTypeId) 
   
        FETCH NEXT FROM My_Cursor  into @tenantTypeId;   --读取下一行数据
    END
CLOSE My_Cursor; --关闭游标
DEALLOCATE My_Cursor; --释放游标

Go


-------用户计数升级
DECLARE @userId nvarchar(60)
DECLARE @pendingCount int
DECLARE @againCount int
DECLARE @successCount int
DECLARE @returnCount int
DECLARE @approvalStatus tinyint
DECLARE My_Cursor CURSOR --定义游标
FOR (SELECT  [UserId] FROM  [tn_Users]) --查出需要的集合放到游标中
OPEN My_Cursor; --打开游标 
FETCH NEXT FROM My_Cursor into @userId;  --读取第一行数据
WHILE @@FETCH_STATUS = 0
    BEGIN
	--贴子计数升级
	set  @returnCount= (select count(*) from  tn_Threads where UserId= @userId )
	set  @successCount= (select count(*) from  tn_Threads where UserId= @userId  and ApprovalStatus = 40)
    set  @againCount= (select count(*) from  tn_Threads where UserId= @userId  and ApprovalStatus = 30)
	set  @pendingCount= (select count(*) from  tn_Threads where UserId= @userId  and ApprovalStatus = 20)
	 	insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Post-TenantTypeId-100002-ThreadCount-UserId-'+@userId,@returnCount,getdate())
	insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Post-TenantTypeId-100002-ThreadCount-UserId-'+@userId+'-AuditStatusSuccess',@successCount,getdate())
	  insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Post-TenantTypeId-100002-ThreadCount-UserId-'+@userId+'-AuditStatusAgain',@againCount,getdate())
	  insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Post-TenantTypeId-100002-ThreadCount-UserId-'+@userId+'-AuditStatusPending',@pendingCount,getdate())


	  --资讯文章计数升级
	set  @returnCount= (select count(*) from  tn_ContentItems where UserId= @userId and tn_ContentItems.ContentModelId = 5 )
	set  @successCount= (select count(*) from  tn_ContentItems where UserId= @userId  and ApprovalStatus = 40 and tn_ContentItems.ContentModelId = 5)
    set  @againCount= (select count(*) from  tn_ContentItems where UserId= @userId  and ApprovalStatus = 30 and tn_ContentItems.ContentModelId = 5)
	set  @pendingCount= (select count(*) from  tn_ContentItems where UserId= @userId  and ApprovalStatus = 20 and tn_ContentItems.ContentModelId = 5)
	 	insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('CMS-ContentModelKey-Contribution-ContentItemCount-UserId-'+@userId,@returnCount,getdate())
	insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('CMS-ContentModelKey-Contribution-ContentItemCount-UserId-'+@userId+'-AuditStatusSuccess',@successCount,getdate())
	  insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('CMS-ContentModelKey-Contribution-ContentItemCount-UserId-'+@userId+'-AuditStatusAgain',@againCount,getdate())
	  insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('CMS-ContentModelKey-Contribution-ContentItemCount-UserId-'+@userId+'-AuditStatusPending',@pendingCount,getdate())
	 
	 
	  --评论计数升级
	set  @returnCount= (select count(*) from  [tn_Comments] where UserId= @userId and TenantTypeId not in('100002') )
	set  @successCount= (select count(*) from  [tn_Comments] where UserId= @userId  and ApprovalStatus = 40 and TenantTypeId not in('100002') )
    set  @againCount= (select count(*) from  [tn_Comments] where UserId= @userId  and ApprovalStatus = 30 and TenantTypeId not in('100002') )
	set  @pendingCount= (select count(*) from  [tn_Comments] where UserId= @userId  and ApprovalStatus = 20 and TenantTypeId not in('100002') )
	 	insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Comment-TenantTypeId--CommentCount-UserId-'+@userId,@returnCount,getdate())
	insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Comment-TenantTypeId--CommentCount-UserId-'+@userId+'-AuditStatusSuccess',@successCount,getdate())
	  insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Comment-TenantTypeId--CommentCount-UserId-'+@userId+'-AuditStatusAgain',@againCount,getdate())
	  insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Comment-TenantTypeId--CommentCount-UserId-'+@userId+'-AuditStatusPending',@pendingCount,getdate())

	    --回贴计数升级
	set  @returnCount= (select count(*) from  [tn_Comments] where UserId= @userId and TenantTypeId  in('100002') )
	set  @successCount= (select count(*) from  [tn_Comments] where UserId= @userId  and ApprovalStatus = 40 and TenantTypeId  in('100002') )
    set  @againCount= (select count(*) from  [tn_Comments] where UserId= @userId  and ApprovalStatus = 30 and TenantTypeId  in('100002') )
	set  @pendingCount= (select count(*) from  [tn_Comments] where UserId= @userId  and ApprovalStatus = 20 and TenantTypeId  in('100002') )
	 	insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Comment-TenantTypeId-100002-CommentCount-UserId-'+@userId,@returnCount,getdate())
	insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Comment-TenantTypeId-100002-CommentCount-UserId-'+@userId+'-AuditStatusSuccess',@successCount,getdate())
	  insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Comment-TenantTypeId-100002-CommentCount-UserId-'+@userId+'-AuditStatusAgain',@againCount,getdate())
	  insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Comment-TenantTypeId-100002-CommentCount-UserId-'+@userId+'-AuditStatusPending',@pendingCount,getdate())
	 

	   --收藏计数升级
	set  @returnCount= (select count(*) from  [tn_Favorites] where UserId= @userId  )
	
	 	insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Favorite-FavoriteCount-UserId-'+@userId,@returnCount,getdate())

	   --点赞计数升级
	set  @returnCount= (select count(*) from  tn_AttitudeRecords where UserId= @userId  )
	
	 	insert   tn_KvStore
      ([Tkey]
      ,[TValue]
      ,[DateCreated]) values('Attitude-AttitudeCount-UserId-'+@userId,@returnCount,getdate())

	  
        FETCH NEXT FROM My_Cursor  into @userId;   --读取下一行数据 
    END
CLOSE My_Cursor; --关闭游标
DEALLOCATE My_Cursor; --释放游标


-- 搜索词计数
insert  tn_Counts (
       [TenantTypeId]
      ,[OwnerId]
      ,[ObjectId]
      ,[CountType]
      ,[StatisticsCount])

SELECT 
      '000141'as  [TenantTypeId]
	  ,[OwnerId]
      ,[ObjectId]
	   ,'SearchWordCounts' as [CountType]
      ,[StatisticsCount]
     
  FROM [tn.jinhudemoNew].[dbo].[tn_Counts_000021]  as tb  where tb.CountType='SearchCount'
  
  insert  tn_Counts (
       [TenantTypeId]
      ,[OwnerId]
      ,[ObjectId]
      ,[CountType]
      ,[StatisticsCount])

SELECT 
      '000141'as  [TenantTypeId]
	  ,[OwnerId]
      ,[ObjectId]
	   ,'SearchWordCounts-180' as [CountType]
      ,[StatisticsCount]
      
  FROM [tn.jinhudemoNew].[dbo].[tn_Counts_000021] as tb  where tb.CountType='SearchCount-7'

 commit tran

   