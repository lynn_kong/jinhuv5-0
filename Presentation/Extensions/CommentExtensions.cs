﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Tunynet.Attitude;

namespace Tunynet.Common
{
    /// <summary>
    /// 评论的扩展类
    /// </summary>
    public static class CommentExtensions
    {
        /// <summary>
        /// 获取评论的用户
        /// </summary>
        /// <returns></returns>
        public static User User(this Comment comement)
        {
            return DIContainer.Resolve<UserService>().GetFullUser(comement.UserId);
        }

        /// <summary>
        /// 判断此评论是否已经被点赞
        /// </summary>
        /// <param name="comment"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static bool? IsSupport(this Comment comment, long userId)
        {
            AttitudeService attitudeService = new AttitudeService(comment.TenantTypeId);
            return attitudeService.IsSupport(comment.Id, userId);
        }

        /// <summary>
        /// 获取当前对象的点赞数
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public static int GetSupportCount(this Comment comment)
        {
            AttitudeService attitudeService = new AttitudeService(comment.TenantTypeId);
            var supportCount = 0;
            var attitude = attitudeService.Get(comment.Id);
            if (attitude != null)
                supportCount = attitude.SupportCount;
            return supportCount;
        }
    }
}