﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using AutoMapper;

namespace Tunynet.Common
{
    public class AutoMapperConfiguration
    {
        /// <summary>
        /// 初始化autompper 配置
        /// </summary>
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<SourceProfile>();
            });
        }
    }
}