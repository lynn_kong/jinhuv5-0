﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Lucene.Net.Documents;
using Lucene.Net.Search;
using System;
using System.Collections.Generic;

namespace Tunynet.Search
{
    /// <summary>
    /// 搜索引擎接口定义
    /// </summary>
    public interface ISearchEngine
    {
        /// <summary>
        /// 重建索引
        /// </summary>
        /// <remarks>
        /// 重建索引数据量大，一般分多次进行调用本方法，用isBeginning和isEndding参数；
        /// 重建索引会重置搜索管理器，搜索服务可能在切换索引时短时中断；
        /// 重建索引的目的是为了避免因意外情况导致索引不完整
        /// </remarks>
        /// <param name="indexDocuments">待添加的内容</param>
        /// <param name="isBeginning">开始重建</param>
        /// <param name="isEndding">完成重建</param>
        void RebuildIndex(IEnumerable<Document> indexDocuments, bool isBeginning, bool isEndding);

        /// <summary>
        /// 添加索引内容
        /// </summary>
        /// <param name="indexDocument">待添加的索引文档</param>
        /// <param name="reopen">是否重新打开NRT查询</param>
        void Insert(Document indexDocument, bool reopen = true);

        /// <summary>
        /// 添加索引内容
        /// </summary>
        /// <param name="indexDocuments">待添加的索引文档</param>
        /// <param name="reopen">是否重新打开NRT查询</param>
        void Insert(IEnumerable<Document> indexDocuments, bool reopen = true);

        /// <summary>
        /// 删除索引内容
        /// </summary>
        /// <param name="id">索引内容对应的实体主键</param>
        /// <param name="fieldNameOfId">实体主键对应的索引字段名称</param>
        /// <param name="reopen">是否重新打开NRT查询</param>
        void Delete(string id, string fieldNameOfId, bool reopen = true);

        /// <summary>
        /// 删除索引内容
        /// </summary>
        /// <param name="ids">索引内容对应的实体主键</param>
        /// <param name="fieldNameOfId">实体主键对应的索引字段名称</param>
        /// <param name="reopen">是否重新打开NRT查询</param>
        void Delete(IEnumerable<string> ids, string fieldNameOfId, bool reopen = true);

        /// <summary>
        /// 更新索引内容
        /// </summary>
        /// <param name="indexDocument">索引文档</param>
        /// <param name="id">实体主键</param>
        /// <param name="fieldNameOfId">实体主键对应的索引字段名称</param>
        /// <param name="reopen">是否重新打开NRT查询</param>
        void Update(Document indexDocument, string id, string fieldNameOfId, bool reopen = true);

        /// <summary>
        /// 更新索引内容
        /// </summary>
        /// <param name="indexDocuments">索引文档</param>
        /// <param name="ids">实体主键</param>
        /// <param name="fieldNameOfId">实体主键对应的索引字段名称</param>
        /// <param name="reopen">是否重新打开NRT查询</param>
        void Update(IEnumerable<Document> indexDocuments, IEnumerable<string> ids, string fieldNameOfId, bool reopen = true);

        /// <summary>
        /// 搜索并返回分页数据
        /// </summary>
        /// <param name="searchQuery"><see cref="Lucene.Net.Search.Query"/></param>
        /// <param name="filter"><see cref="Lucene.Net.Search.Filter"/></param>
        /// <param name="sort"><see cref="Lucene.Net.Search.Sort"/></param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="pageSize">每页记录数</param>
        /// <returns>内容为<see cref="Lucene.Net.Documents.Document"/>的分页数据集合</returns>
        PagingDataSet<Document> Search(Query searchQuery, Filter filter, Sort sort, int pageIndex, int pageSize);

        /// <summary>
        /// 搜索并返回数据
        /// </summary>
        /// <param name="searchQuery"><see cref="Lucene.Net.Search.Query"/></param>
        /// <param name="filter"><see cref="Lucene.Net.Search.Filter"/></param>
        /// <param name="sort"><see cref="Lucene.Net.Search.Sort"/></param>
        /// <returns></returns>
        IEnumerable<Document> Search(Query searchQuery, Filter filter, Sort sort);

        /// <summary>
        /// 搜索并返回前topNumber条数据
        /// </summary>
        /// <param name="searchQuery"><see cref="Lucene.Net.Search.Query"/></param>
        /// <param name="filter"><see cref="Lucene.Net.Search.Filter"/></param>
        /// <param name="sort"><see cref="Lucene.Net.Search.Sort"/></param>
        /// <param name="topNumber">最多返回多少条数据</param>
        /// <returns></returns>
        IEnumerable<Document> Search(Query searchQuery, Filter filter, Sort sort, int topNumber);

        /// <summary>
        /// 获取当前搜索引擎的索引大小，单位为字节(Byte)
        /// </summary>
        double GetIndexSize();

        /// <summary>
        /// 获取友好格式索引大小
        /// </summary>
        /// <returns></returns>
        string GetFriendlyIndexSize();

        /// <summary>
        /// 获取当前搜索引擎的索引最后更新时间
        /// </summary>
        DateTime GetLastModified();

        /// <summary>
        /// 提交索引变更
        /// </summary>
        void Commit();
    }
}